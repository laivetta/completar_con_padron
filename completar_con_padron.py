# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 16:31:11 2020

@author: Usuario
"""

import pandas as pd
import time
import xlrd


"""
Este script lo que hace es ponerle todos los numeros de inscripcion que tiene
el archivo padron.xlsx (que salio del adhoc analisis - sujetos pasivos - 
                        datos por cuit) a este archivo no hay que cambiarle 
nada de como fue emitido por adhoc, solo dejarle IB y CM
En el archivo original.xlsx es el que se va a completar, lo unico que tiene
que tener es la primer colomna con "Cuit"


Controlar en la salida del script que la ultima columna debe ser Both, esto
indica que encontro el cuit en la salida padron, si solo sale Left quiere decir
que el cuit no fue encontrado en la salida padron

"""




#-------------------- LECTURA DE LOS ARCHIVOS-----------------------------
print()
print ("comienzo lectura archivos")
inicio_lectura =  time.time()

periodo_cuota = pd.read_excel ("original.xlsx")
periodo_cuota ["Cuit"] = periodo_cuota["Cuit"].map(str)

padron = pd.read_excel("padron.xlsx", header=2)



print ("fin lectura archivos")
fin_lectura = time.time()
calculo_segundos = fin_lectura - inicio_lectura
print (f"--- {calculo_segundos:.2f} segundos leyendo archivos ---")
print()
#------------------TRANSFORMACION BASICAS ----------------------------------

print("comienzo transformaciones dataframes")

padron["CUIT"] = padron["SPO_CUIT"].str.replace('-','')
padron["CUIT"] = padron["CUIT"].map(str)
padron = padron.loc[:, ["CUIT", "INS_NUMERO_INSCRIPCION"]]  


print ("fin transformaciones dataframe")
print()


# ---------------------- LEFT JOIN -------------------------------------------
print ("comienzo de LEFT JOIN")

combinado= pd.merge(periodo_cuota,
                    padron,
                    how = "left",
                    left_on="Cuit",
                    right_on="CUIT",
                    indicator="indicador")


print ("fin de LEFT JOIN")
print()

#-------------------------- GENERACION DE SALIDA -----------------------------
print("comienzo generacion excel")
inicio_escritura = time.time()

with pd.ExcelWriter('Completado con isib.xlsx') as writer:
    combinado.to_excel(writer,
                       sheet_name="RESUMEN",
                       index = False)


print ("fin generacion excel" )
fin__escritura = time.time()
calculo_segundos = fin__escritura - inicio_escritura
print (f"--- {calculo_segundos:.2f} segundos generando archivos ---")
